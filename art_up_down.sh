#!/bin/bash

# Set variables for the Artifactory credentials and URL
ARTIFACTORY_USER=myuser
ARTIFACTORY_PASSWORD=mypassword
ARTIFACTORY_URL=https://myartifactory.com

# Download the latest artifact from Artifactory
curl -u ${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD} \
    "${ARTIFACTORY_URL}/api/search/latestVersion?g=group1&g=group2&a=artifact1&a=artifact2&repos=reponame1&repos=reponame2" \
    -o "${HOME}/Desktop/latest_artifact.txt"

# Search for matching files in the current directory
for file in $(ls); do
    if [[ "$file" == "latest_artifact.txt" ]]; then
        echo "Matching file found: $file"
        # Deploy the JSON artifact to multiple Artifactory repositories
        curl -u ${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD} \
        -X PUT "${ARTIFACTORY_URL}/api/deploy/reponame1/path/to/artifact.json" \
        -H "Content-Type: application/json" \
        -T "${HOME}/Desktop/latest_artifact.txt"

        curl -u ${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD} \
        -X PUT "${ARTIFACTORY_URL}/api/deploy/reponame2/path/to/artifact.json" \
        -H "Content-Type: application/json" \
        -T "${HOME}/Desktop/latest_artifact.txt"/