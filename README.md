# Playwright_Docker_Project

Push image to Artifactory Registry 

```
docker push ${ARTIFACTORY_URL}/${ARTIFACTORY_USER}/${ARTIFACTORY_PASSWORD}/my-playwright-container:latest
```

or 
```
docker push ${ARTIFACTORY_URL}/${ARTIFACTORY_USER}/${ARTIFACTORY_PASSWORD}/my-playwright-container:1.0
```

ensure 
docker login ${ARTIFACTORY_URL} -u ${ARTIFACTORY_USER} -p ${ARTIFACTORY_PASSWORD}


## Getting started
```
echo "0 2 * * 7 /usr/bin/curl -u ${ARTIFACTORY_USER}:${ARTIFACTORY_PASSWORD} "${ARTIFACTORY_URL}/api/search/latestVersion?g=group1&g=group2&a=artifact1&a=artifact2&repos=reponame1&repos=reponame2" -o "${HOME}/Desktop/latest_artifact.txt" -s -f -w "HTTP Code: %{http_code}\\n" >> /var/log/cron.log 2>&1 || echo "$(date): Failed to download artifact" >> /var/log/cron.log" | crontab -
```
