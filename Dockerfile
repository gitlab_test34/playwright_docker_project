FROM python:3.9-alpine

# Add the necessary dependencies
RUN apk add --no-cache \
    chromium \
    ffmpeg \
    ttf-freefont \
    nodejs \
    npm

# Copy Requirements.txt 
COPY requirements.txt /app/

RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt
RUN pip install playwright
RUN playwright install

# Create a directory to mount the volume
RUN mkdir /app

# Expose the necessary ports
EXPOSE 80
EXPOSE 443

# Mount the volume and set the working directory
VOLUME /app
WORKDIR /app

# Set the entrypoint to run your script
ENTRYPOINT ["python3", "-m", "pytest"]

# Tag the image with the right repository
# ARG ARTIFACTORY_URL
# ARG ARTIFACTORY_USER
# ARG ARTIFACTORY_PASSWORD
# ENV ARTIFACTORY_URL=${ARTIFACTORY_URL}
# ENV ARTIFACTORY_USER=${ARTIFACTORY_USER}
# ENV ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD}
# RUN echo ${ARTIFACTORY_URL}/${ARTIFACTORY_USER}/${ARTIFACTORY_PASSWORD}
# LABEL repository=${ARTIFACTORY_URL}/${ARTIFACTORY_USER}/${ARTIFACTORY_PASSWORD}
# docker build -t my-playwright-container .
# docker run -v /path/to/your/script:/app -it my-playwright-container
